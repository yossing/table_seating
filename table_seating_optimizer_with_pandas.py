import random
import numpy as np
import pandas as pd
from simanneal import Annealer
# class SeatingOptimizer(object):

def get_info_pd(excel_path):
    info_pd = pd.read_excel(excel_path)
    return info_pd
def optimize(guest_info):
    so = SeatingOptimizer(guest_info)
    so.anneal()
    return so.guest_info



class SeatingOptimizer(Annealer):

    """Optimize seating plan at single table given preferences"""
    guest_weight = None
    max_neighbours = None
    n_seats = None
    names = []
    neighbours = {}
    preferences = {}
    score = None
    max_score = None
    num_places = None
    seating = {}
    
    def __init__(self, guest_info):
        """
        @params: names - list of names
                 preferences - dict where each key is a name in names, 
                  and each val is a list of preferences (also in names)
                 guests - dict where each key is a name in namea and each
                          val is number indicating the number of guests 
                          for that name.
        """


        self.guest_weight = 50
        self.pref_weight = 1
        self.max_neighbours = 3
        self.max_score = 1
        self.score = 1
        # self.names = names
        # self.preferences = preferences
        # self.guests = guests
        total_guests = 0
        
        # for name,num_guests in guests.items():
        #     for ii in range(num_guests):
        #         names.append(name+'_guest')
   
        self.n_places = len(guest_info) 
        self.guest_info = self.format_guest_info(guest_info)
        self.total_max_score = self.guest_info.max_score.sum()

        self.init_seating()
        seating_order = np.ndarray.tolist(self.guest_info['seat_num'].values)


        self.state = seating_order
        
        # self.Tmax = self.max_score
        # self.Tmin = 2
        # self.steps = 10000
        # self.updates = 10e12
        
        self.copy_strategy = 'slice'
        super(SeatingOptimizer, self).__init__(self.state)  # important!
        
    def format_guest_info(self, guest_info):
        '''
        Add relevant rows to dataframe
        '''
        n_entries = len(guest_info)
        guest_info['max_score'] = np.zeros(n_entries, dtype=int)
        guest_info['num_guests'] = np.zeros(n_entries, dtype=int)
        guest_info['seat_number'] = np.arange(1,n_entries+1)
    #     for k in guest_info.keys():
    #         guest_info[k].str.lower()
        max_neighbours = self.max_neighbours
        guest_info.Name = guest_info.Name.str.lower()
        
        for ii in range(max_neighbours):
            guest_info['pref_'+str(ii+1)] = guest_info['pref_'+str(ii+1)].str.lower()    
        
        for ix, row in guest_info.iterrows():
            this_name = row.Name
            if 'guest' not in this_name:

                this_num_guests = guest_info.Name.str.count(this_name).sum() - 1
                row['num_guests'] = this_num_guests

                neighbour_count = this_num_guests

                #add each guest score to max score for this entry
                row['max_score'] = self.guest_weight*this_num_guests
                
                #Make the each guest an automatic preference
                for ii in range(this_num_guests):
                    n = ii+1
                    row['pref_'+str(n)] = this_name + ' guest'
                for ii in range(max_neighbours - this_num_guests):
                    n = ii+this_num_guests+1
                    if not pd.isnull(row['pref_'+str(n)]):
                        row['max_score'] += self.pref_weight  
            else:
                row['pref_1'] = this_name.replace('guest','')
                row['max_score'] = self.guest_weight
    #         print(row)
            guest_info.iloc[ix] = row

        return guest_info

    def find_best_pref_match(self, pref):
        names_series = self.guest_info.Name.copy()





    def is_neighbour(self, n1,n2,n_tot):
        print(n1)
        print(n2)
        neighbours = False
        s1,s2 = self.split_sides(n_tot)

        for s in [s1,s2]:
            n1_ix = np.argwhere(s==n1)
            n2_ix = np.argwhere(s==n2)
            #make sure they are both sitting on the same side
            if len(n1_ix)>0 and len(n2_ix)>0:
                n1_ix = int(n1_ix)
                n2_ix = int(n2_ix)
                if abs(n1_ix-n2_ix) == 1:
                    neighbours = True
        return neighbours


    def is_opposite(self, n1,n2,n_tot):
        opposite = False
        s1,s2 = self.split_sides(n_tot)

        n1_ix = np.argwhere(s1==n1)
        n2_ix = np.argwhere(s2==n2)

        if n1_ix==n2_ix and len(n1_ix)>0 and len(n2_ix)>0:
            opposite==True
        n1_ix = np.argwhere(s2==n1)
        n2_ix = np.argwhere(s1==n2)
        if n1_ix==n2_ix and len(n1_ix)>0 and len(n2_ix)>0:
            opposite==True
        return opposite

    
    def init_seating(self):
        if 'seat_num' not in self.guest_info:
            self.guest_info['seat_num'] = np.arange(1,len(self.guest_info)+1, dtype=int)
            
        seating_order = self.guest_info['seat_num'].copy()
        random.shuffle(seating_order)
        self.guest_info['seat_num'].map(seating_order)

    
    def split_sides(self, n_places):
        s1 = np.arange(1,int(np.ceil(n_places/2)))
        s2 = np.arange(int(np.ceil(n_places/2)),n_places+1)
        return s1,s2

    def energy(self):
        """"Calculates the energy of a given configuration. 
        Score is increased by pref_weight for each name sitting next to their preference. 
        Score is increased by guest_weight for each name sitting next to their guest"""
        seating_order = self.state
        self.guest_info['seat_num'] = seating_order

        new_score = 0
        for ix, row in self.guest_info.iterrows():
            seat_num = row['seat_num']
            for ii in range(self.max_neighbours):
                pref = row['pref_'+str(ii+1)]
                if not pd.isnull(pref):
                    print(pref)
                    print(self.guest_info.where(self.guest_info['Name'] == pref).seat_num)
                    pref_seat_num = self.guest_info.where(self.guest_info['Name'] == pref).seat_num
                else:
                    pref_seat_num = -1
                if self.is_neighbour(pref_seat_num, seat_num, self.n_places):
                    if 'guest' in pref:
                        new_score += self.guest_weight
                    else:
                        new_score += self.pref_weight
                if self.is_opposite(pref_seat_num, seat_num, self.n_places):
                    if 'guest' in pref:
                        new_score += self.guest_weight
                    else:
                        new_score += self.pref_weight
        new_score = 1/(1+new_score)
        return new_score

    
    # def move(self):
    #     """Swaps two names in seating order."""
    #     a = random.randint(0, len(self.state) - 1)
    #     b = random.randint(0, len(self.state) - 1)

    #     seat_num_a = self.state.iloc[a]['seat_num']
    #     seat_num_b = self.state.iloc[b]['seat_num']

    #     self.state.iloc[a]['seat_num'] = seat_num_b
    #     self.state.iloc[b]['seat_num'] = seat_num_a
        
    def move(self):
        """Swaps two names in seating order."""
        a = random.randint(0, len(self.state) - 1)
        b = random.randint(0, len(self.state) - 1)
        self.state[a], self.state[b] = self.state[b], self.state[a]

        
    
