import random
import numpy as np
from simanneal import Annealer
# class SeatingOptimizer(object):
class SeatingOptimizer(Annealer):

    """Optimize seating plan at single table given preferences"""
    guest_weight = None
    max_neighbours = None
    n_seats = None
    names = []
    neighbours = {}
    preferences = {}
    score = None
    max_score = None
    num_places = None
    seating = {}
    
    def __init__(self, names, preferences, guests):
        """
        @params: names - list of names
                 preferences - dict where each key is a name in names, 
                  and each val is a list of preferences (also in names)
                 guests - dict where each key is a name in namea and each
                          val is number indicating the number of guests 
                          for that name.
        """
        self.guest_weight = 50
        self.max_neighbours = 3
        self.score = 1
        self.names = names
        self.preferences = preferences
        self.guests = guests
        total_guests = 0
        
        for name,num_guests in guests.items():
            for ii in range(num_guests):
                names.append(name+'_guest')
   
        self.num_places = len(names) 
        self.max_score = self.calculate_max_score()
        self.state = self.init_seating()
        
        # self.Tmax = self.max_score
        # self.Tmin = 2
        # self.steps = 10000
        # self.updates = 10e12
        
        self.copy_strategy = 'slice'
        super(SeatingOptimizer, self).__init__(self.state)  # important!
        
        
    def calculate_max_score(self):
        guests = self.guests
        guest_weight = self.guest_weight
        preferences = self.preferences
        max_score = self.max_score
        name_max_score = {}
        for name in self.names:
            this_max_score = 0
            neighbour_count = 0
            if name in guests.keys():
                num_guests = guests[name]
                neighbour_count += num_guests
                this_max_score += num_guests*guest_weight 

            if name in preferences.keys():
                these_prefs = preferences[name]
                #at best, each guest can sit next to up to three guests
                this_max_score += np.min([len(these_prefs), self.max_neighbours - neighbour_count])
            name_max_score[name] = this_max_score
            max_score += this_max_score
        self.name_max_score = name_max_score
        return max_score
    
    def init_seating(self):
        seating_order = self.names
        random.shuffle(seating_order)
        return seating_order
    
    def split_sides(self, seating_order):
        s1 = []
        s2 = []
        for ii,name in enumerate(seating_order):
            if ii%2==0:
                s1.append(name)
            else:
                s2.append(name)
        return s1,s2
        
    def energy(self):
        """"Calculates the energy of a given configuration. 
        Score is increased by 1 for each name sitting next to their preference. 
        Score is increased by guest_weight for each name sitting next to their guest"""
        
        new_score = 0
        names = self.names
        preferences = self.preferences
        seating_order = self.state
        guest_weight = self.guest_weight
        
        s1,s2 = self.split_sides(seating_order)

        def get_name_neighbours(s1,s2):
            name_neighbours = {}
            num_s1 = len(s1)-1
            num_s2 = len(s2)-1
            for ii, name in enumerate(s1):
                neighbours = []
                if ii>0:
                    neighbours.append(s1[ii-1])
                if ii < num_s1:
                    neighbours.append(s1[ii+1])
                if ii <= num_s2:
                    neighbours.append(s2[ii])
                name_neighbours[name] = neighbours
            return name_neighbours
        
        name_neighbours = get_name_neighbours(s1,s2)
        name_neighbours.update(get_name_neighbours(s2,s1))
        name_score = {}
        for name, neighbours in name_neighbours.items():
            this_name_score = 0
            for neighbour in neighbours:
                if neighbour == name + '_guest' or name == neighbour+'_guest':
                    this_name_score += guest_weight
                if name in preferences.keys():
                    for pref in preferences[name]:
                        if pref == neighbour:
                            this_name_score += 1
            name_score[name] = this_name_score
            new_score += this_name_score
        self.name_neighbours = name_neighbours
        self.name_score = name_score

        #Want energy to decrease with improvement
        e = 1/(1+new_score)
        return e
           
#         num_seats = len(seating_order)
#         for ii,name in enumerate(seating_order):
#             #move through table from one side to the other in zigzag as in: _-_-_-_-_-
#             #define neighbours: n-2, n+1, n+2
#             neighbours = []
#             if ii > 1:
#                 neighbours.append(seating_order[ii-2])
#             if ii < num_seats - 2:
#                 neighbours.append(seating_order[ii+2])
#             if ii < num_seats-1:
#                 neighbours.append(seating_order[ii+1])
#             for neighbour in neighbours:
#                 if neighbour == name + '_guest':
#                     new_score += guest_weight
#                 if name in preferences.keys():
#                     for pref in preferences[name]:
#                         if pref == neighbour:
#                             new_score += 1
#         return new_score
    
    def move(self):
        """Swaps two names in seating order."""
        a = random.randint(0, len(self.state) - 1)
        b = random.randint(0, len(self.state) - 1)
        self.state[a], self.state[b] = self.state[b], self.state[a]
        
#     def anneal(self):
        
#         for ii in range(self.steps):
#             old_seating_order = self.seating_order.copy()
#             self.move()
#             new_score = self.energy()
#             if new_score > self.score:
#                 self.score = new_score
#             else:
#                 self.seating_order = old_seating_order
#             if ii % self.updates == 0:
#                 print("Iteration %i" %ii)
#                 print('Score = %i' %self.score)  
#         return self.seating_order, self.score

        
    
